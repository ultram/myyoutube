package com.codemobi.myyoutube;

import java.util.List;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class YoutubeAdapter extends ArrayAdapter<Video> {
	private int resource;

	public YoutubeAdapter(Context context, int resource, List<Video> objects) {
		super(context, resource, objects);
		this.resource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout youtubeView;
		Video item = getItem(position);

		if (convertView == null) {
			youtubeView = new RelativeLayout(getContext());
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			vi.inflate(resource, youtubeView, true);
		} else {
			youtubeView = (RelativeLayout)convertView;
		}
		
		TextView textViewTitle = (TextView)youtubeView.findViewById(R.id.textViewTitle);
		TextView textViewUser = (TextView)youtubeView.findViewById(R.id.textViewUser);
		ImageView imageViewIcon = (ImageView)youtubeView.findViewById(R.id.imageViewIcon);
		
		textViewTitle.setText(item.getTitle());
		textViewUser.setText(item.getAuthor());
		Picasso.with(getContext()).load(item.getThumbnailUrl()).into(imageViewIcon);

		return youtubeView;
	}

}
