package com.codemobi.myyoutube;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

public class MainActivity extends Activity {
	public static final String MY_PREF = "MYPREF";
	
	private static final String TAG = "MyYoutube";
	private AsyncHttpClient client = new AsyncHttpClient();
	private static final String SEARCH_URL = "https://gdata.youtube.com/feeds/api/videos?orderby=published&v=2&alt=json&q=";
	private ArrayList<Video> youtubeItems = new ArrayList<Video>();
	private ArrayAdapter<Video> youtubeAdapter;

	private EditText editTextSearch;
	private ListView listViewYoutube;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		listViewYoutube = (ListView) findViewById(R.id.listViewYoutube);

		youtubeAdapter = new YoutubeAdapter(this, R.layout.youtube_item,
				youtubeItems);
		listViewYoutube.setAdapter(youtubeAdapter);
		listViewYoutube.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Video videoItem = youtubeItems.get(position);
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoItem.getYoutubeUrl()));
				startActivity(intent);
			}
		});
		
		editTextSearch = (EditText)findViewById(R.id.editTextSearch);
		editTextSearch.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER &&
					event.getAction() == KeyEvent.ACTION_DOWN) {
					search(editTextSearch.getText().toString());
					return true;
				}
				return false;
			}
		});
		
		Button buttonSearch = (Button)findViewById(R.id.buttonSearch);
		buttonSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				search(editTextSearch.getText().toString());
			}
		});
		
		SharedPreferences mySharedPreferences = getSharedPreferences(MY_PREF, Activity.MODE_PRIVATE);
		String oldSearch = mySharedPreferences.getString("SEARCH", "godzilla");
		editTextSearch.setText(oldSearch);
		search(oldSearch);
	}
	
	private void search(String q) {
		SharedPreferences mySharedPreferences = getSharedPreferences(MY_PREF, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("SEARCH", q);
		editor.commit();
		
		youtubeItems.clear();
		String youtubeUrl = SEARCH_URL + q;
		client.get(youtubeUrl, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(JSONObject response) {
				try {
					JSONObject feed = response.getJSONObject("feed");
					JSONArray entries = feed.getJSONArray("entry");
					for (int i = 0; i < entries.length(); i++) {
						JSONObject entry = entries.getJSONObject(i);
						String title = entry.getJSONObject("title").getString(
								"$t");
						Log.d(TAG, "Title : " + title);
						String author = entry.getJSONArray("author")
								.getJSONObject(0).getJSONObject("name")
								.getString("$t");

						String thumbnailUrl = entry
								.getJSONObject("media$group")
								.getJSONArray("media$thumbnail")
								.getJSONObject(0).getString("url");

						String youtubeUrl = entry.getJSONArray("link")
								.getJSONObject(0).getString("href");

						Video videoItem = new Video(title, author,
								thumbnailUrl, youtubeUrl);
						
						youtubeItems.add(videoItem);
						youtubeAdapter.notifyDataSetChanged();
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

		});
	}
}
