package com.codemobi.myyoutube;

public class Video {
	private String title;
	private String author;
	private String thumbnailUrl;
	private String youtubeUrl;
	
	public Video(String title, String author, String thumbnailUrl,
			String youtubeUrl) {
		super();
		this.title = title;
		this.author = author;
		this.thumbnailUrl = thumbnailUrl;
		this.youtubeUrl = youtubeUrl;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getThumbnailUrl() {
		return thumbnailUrl;
	}

	public String getYoutubeUrl() {
		return youtubeUrl;
	}
	
	
}
